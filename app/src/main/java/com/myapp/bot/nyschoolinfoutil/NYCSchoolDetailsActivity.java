package com.myapp.bot.nyschoolinfoutil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myapp.bot.nyschoolinfoutil.adapters.NYCSchoolsDetailsAdapter;
import com.myapp.bot.nyschoolinfoutil.model.AppVars;
import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolSATScoresModel;
import com.myapp.bot.nyschoolinfoutil.network.ConnectionCheck;
import com.myapp.bot.nyschoolinfoutil.viewmodel.NYCSchoolDetailsViewModel;

import java.util.List;
import java.util.logging.Logger;

public class NYCSchoolDetailsActivity extends AppCompatActivity implements NYCSchoolsDetailsAdapter.SATListClickListener {

    Logger logger = Logger.getLogger(this.getClass().getName());
    private NYCSchoolDetailsViewModel viewSATModel;
    private List<NYCSchoolSATScoresModel> nycNYCSchoolSATScoresModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_n_y_c_school_info);

        //To identify the school's SAT score based on unique key value "Dbn"
        Intent intent = getIntent();
        String dbn = intent.getStringExtra("dbn");

        logger.info("DBN received: " + dbn);

        RecyclerView recyclerView = findViewById(R.id.recyclersatView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final TextView noresultSat = findViewById(R.id.noResultSAT);

        NYCSchoolsDetailsAdapter satAdapter = new NYCSchoolsDetailsAdapter(this, nycNYCSchoolSATScoresModelList, this);
        recyclerView.setAdapter(satAdapter);

        //To Display SAT results for each NYC school
        viewSATModel = new ViewModelProvider(this).get(NYCSchoolDetailsViewModel.class);
        viewSATModel.getSatListObservor().observe(this, SATScoresModel -> {
            logger.info("The SAT score model is : " + SATScoresModel);
            if (SATScoresModel != null) {
                nycNYCSchoolSATScoresModelList = SATScoresModel;
                satAdapter.setSATList(SATScoresModel);
                noresultSat.setVisibility(View.GONE);
            } else {
                noresultSat.setVisibility(View.VISIBLE);
            }
        });

        //@Test: Check for Network Connectivity
        ConnectionCheck conCheck = new ConnectionCheck(getApplicationContext());
        conCheck.isNetworkConnected();

        if (conCheck.isNetworkConnected()) {
            Toast.makeText(getApplicationContext(), "Network Connected!", Toast.LENGTH_LONG).show();
            viewSATModel.makeApiCall(dbn);
        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemClick(NYCSchoolSATScoresModel NYCSchoolSatScoresModel) {

    }
}