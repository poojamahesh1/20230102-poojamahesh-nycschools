package com.myapp.bot.nyschoolinfoutil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myapp.bot.nyschoolinfoutil.adapters.NYCSchoolsListAdapter;
import com.myapp.bot.nyschoolinfoutil.model.AppVars;
import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolModel;
import com.myapp.bot.nyschoolinfoutil.network.ConnectionCheck;
import com.myapp.bot.nyschoolinfoutil.viewmodel.NYCSchoolListViewModel;

import java.util.List;
import java.util.logging.Logger;

public class NYCSchoolListActivity extends AppCompatActivity implements NYCSchoolsListAdapter.NYCSchoolsListClickListener {

    Logger logger = Logger.getLogger(this.getClass().getName());

    private NYCSchoolListViewModel viewModel;
    private List<NYCSchoolModel> nycSchoolsModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("NYC Schools List");

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final TextView nycSchools = findViewById(R.id.nycSchoolsTv);


        NYCSchoolsListAdapter adapter = new NYCSchoolsListAdapter(this, nycSchoolsModelList, this);
        recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(this).get(NYCSchoolListViewModel.class);
        viewModel.getSchoolListObservor().observe(this, nySchoolsModels -> {
            if (nySchoolsModels != null) {
                nycSchoolsModelList = nySchoolsModels;
                adapter.setSchoolList(nySchoolsModels);
                nycSchools.setVisibility(View.GONE);
            } else {
                nycSchools.setVisibility(View.VISIBLE);
            }
        });

        ConnectionCheck conCheck = new ConnectionCheck(getApplicationContext());
        if (conCheck.isNetworkConnected()) {
            Toast.makeText(getApplicationContext(), "Network Connected!", Toast.LENGTH_LONG);
            viewModel.makeApiCall();

        } else {
            Toast.makeText(getApplicationContext(), "No Network Connection!", Toast.LENGTH_LONG);
        }

    }

    @Override
    public void onItemClick(NYCSchoolModel nycSchoolsModel) {
        Intent intent = new Intent(NYCSchoolListActivity.this, NYCSchoolDetailsActivity.class);
        intent.putExtra("dbn", nycSchoolsModel.getDbn());
        //logger.info("Added following object :" + nycSchoolsModel);
        startActivity(intent);
    }
}