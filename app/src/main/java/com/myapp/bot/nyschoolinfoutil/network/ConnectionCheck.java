package com.myapp.bot.nyschoolinfoutil.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;

import com.myapp.bot.nyschoolinfoutil.model.AppVars;

import java.util.logging.Logger;

public class ConnectionCheck {
    Context context;
    Logger logger = Logger.getLogger(this.getClass().getName());
    public ConnectionCheck(Context applicationContext) {
        this.context = applicationContext;
    }

    //  This works for android api 26 and above.
    public boolean isNetworkConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities  networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
        if(networkCapabilities != null){
            logger.info("Network connection!");
            return true;
        }
        logger.info("No network connection");
        return false;
    }

}
