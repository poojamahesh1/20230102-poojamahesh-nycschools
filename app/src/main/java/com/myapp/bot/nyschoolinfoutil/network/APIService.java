package com.myapp.bot.nyschoolinfoutil.network;

import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolModel;
import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolSATScoresModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("s3k6-pzi2.json")
    Call<List<NYCSchoolModel>> getSchoolList();

    @GET("f9bf-2cp4.json")
    Call<List<NYCSchoolSATScoresModel>> getSchoolSATList(@Query("dbn")String dbn);
}
