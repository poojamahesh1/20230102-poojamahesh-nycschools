package com.myapp.bot.nyschoolinfoutil.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.ImageView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myapp.bot.nyschoolinfoutil.R;
import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolModel;

import java.util.List;

public class NYCSchoolsListAdapter extends RecyclerView.Adapter<NYCSchoolsListAdapter.MyViewHolder> {

    private List<NYCSchoolModel> nycModelList;
    private NYCSchoolsListClickListener clickListener;
    private Context context;

    public NYCSchoolsListAdapter(Context context, List<NYCSchoolModel> nycModelList, NYCSchoolsListClickListener clickListener) {
        this.nycModelList = nycModelList;
        this.clickListener = clickListener;
        this.context = context;

    }

    @NonNull
    @Override
    public NYCSchoolsListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NYCSchoolsListAdapter.MyViewHolder holder, int position) {
        holder.nycSchoolName.setText(nycModelList.get(position).getSchool_name());
        holder.nycSchoolAddress.setText("Address:" + nycModelList.get(position).getLocation());
        holder.phone.setText("Phone:" + nycModelList.get(position).getPhone_number());
        holder.emailId.setText("Email:" + nycModelList.get(position).getSchool_email());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(nycModelList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if(this.nycModelList != null) {
            return this.nycModelList.size();
        }
        return 0;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nycSchoolName;
        TextView nycSchoolAddress;
        TextView phone;
        TextView emailId;
        ImageView thumbImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            nycSchoolName = itemView.findViewById(R.id.nycSchoolName);
            nycSchoolAddress = itemView.findViewById(R.id.nycSchoolAddress);
            phone = itemView.findViewById(R.id.phone);
            emailId = itemView.findViewById(R.id.email);
           // thumbImage = itemView.findViewById(R.id.thumbImage);
        }
    }

    public interface NYCSchoolsListClickListener {
        void onItemClick(NYCSchoolModel nycSchoolsModel);
    }

    public void setSchoolList(List<NYCSchoolModel> nycModelList) {
        this.nycModelList = nycModelList;
        notifyDataSetChanged();
    }

}


