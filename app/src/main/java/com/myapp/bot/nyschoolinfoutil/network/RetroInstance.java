package com.myapp.bot.nyschoolinfoutil.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroInstance {



    public static String SAT_BASE_URL = "https://data.cityofnewyork.us/resource/";

    private static Retrofit retrofit;
    private static Retrofit retrofit_sat;

    public static Retrofit getRetroClient(String baseURL) {

        if(retrofit == null ) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
