package com.myapp.bot.nyschoolinfoutil.viewmodel;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkRequest;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.myapp.bot.nyschoolinfoutil.model.AppVars;
import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolSATScoresModel;
import com.myapp.bot.nyschoolinfoutil.network.APIService;
import com.myapp.bot.nyschoolinfoutil.network.RetroInstance;

import java.util.List;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NYCSchoolDetailsViewModel extends ViewModel {
    private MutableLiveData<List<NYCSchoolSATScoresModel>> satList;

    public NYCSchoolDetailsViewModel(){
        satList = new MutableLiveData<>();
    }

    Logger logger = Logger.getLogger(this.getClass().getName());

    public MutableLiveData<List<NYCSchoolSATScoresModel>> getSatListObservor() {
        return satList;

    }
    /*
      Connection to Web for retrieving the NYC School SAT scores
      value based on school dbn
     */
    public void makeApiCall(String dbn) {

        APIService apiService = RetroInstance.getRetroClient(AppVars.NYC_SCHOOL_DETAILS_BASE_URL).create(APIService.class);
        Call<List<NYCSchoolSATScoresModel>> call_sat = apiService.getSchoolSATList(dbn);
        call_sat.enqueue(new Callback<List<NYCSchoolSATScoresModel>>() {
            @Override
            public void onResponse(Call<List<NYCSchoolSATScoresModel>> call, Response<List<NYCSchoolSATScoresModel>> response) {
                logger.info("The value retreived from the web is "+response.body());
                satList.postValue(response.body());
            }

            //Condition check upon failure while connecting to the web
            @Override
            public void onFailure(Call<List<NYCSchoolSATScoresModel>> call, Throwable t) {
                logger.info("Exception while making an a call :  "+t.getMessage());
                satList.postValue(null);
            }
        });
    }
}