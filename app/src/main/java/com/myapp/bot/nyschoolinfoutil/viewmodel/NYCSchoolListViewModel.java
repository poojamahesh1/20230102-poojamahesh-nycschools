package com.myapp.bot.nyschoolinfoutil.viewmodel;


import static com.myapp.bot.nyschoolinfoutil.model.AppVars.NYC_SCHOOL_LIST_BASE_URL;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolModel;
import com.myapp.bot.nyschoolinfoutil.network.APIService;
import com.myapp.bot.nyschoolinfoutil.network.RetroInstance;

import java.util.List;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NYCSchoolListViewModel extends ViewModel {

    private MutableLiveData<List<NYCSchoolModel>> schoolList;

    public NYCSchoolListViewModel(){
        schoolList = new MutableLiveData<>();
    }

    Logger logger = Logger.getLogger(this.getClass().getName());

    public MutableLiveData<List<NYCSchoolModel>> getSchoolListObservor() {
        return schoolList;

    }

    //Check for the data connecting to the web
    public void makeApiCall() {
        APIService apiService = RetroInstance.getRetroClient(NYC_SCHOOL_LIST_BASE_URL).create(APIService.class);
        Call<List<NYCSchoolModel>> call = apiService.getSchoolList();
        call.enqueue(new Callback<List<NYCSchoolModel>>() {
            @Override
            public void onResponse(Call<List<NYCSchoolModel>> call, Response<List<NYCSchoolModel>> response) {
                logger.info("The value retreived from the web is "+response.body());
                schoolList.postValue(response.body());
            }

            //Condition check upon failure while connecting to the web
            @Override
            public void onFailure(Call<List<NYCSchoolModel>> call, Throwable t) {
                logger.info("Exception while making an a call :  "+t.getMessage());
                schoolList.postValue(null);
            }
        });
    }
}
