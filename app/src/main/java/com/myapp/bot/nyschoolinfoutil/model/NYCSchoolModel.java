package com.myapp.bot.nyschoolinfoutil.model;

public class NYCSchoolModel {
    private String school_name;
    private String location;
    private String phone_number;
    private String school_email;
    private String dbn;

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    // POJOs
    @Override
    public String toString() {
        return "NYCSchoolModel{" +
                "school_name='" + school_name + '\'' +
                ", location='" + location + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", school_email='" + school_email + '\'' +
                '}';
    }


}

