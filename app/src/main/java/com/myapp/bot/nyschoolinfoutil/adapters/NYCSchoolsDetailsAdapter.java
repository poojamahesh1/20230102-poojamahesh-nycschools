package com.myapp.bot.nyschoolinfoutil.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myapp.bot.nyschoolinfoutil.R;
import com.myapp.bot.nyschoolinfoutil.model.NYCSchoolSATScoresModel;

import java.util.List;

public class NYCSchoolsDetailsAdapter extends RecyclerView.Adapter<NYCSchoolsDetailsAdapter.MyViewHolder> {

   private List<NYCSchoolSATScoresModel> satModelList;
   private SATListClickListener clickListener;
   private Context context;

    public NYCSchoolsDetailsAdapter(Context context, List<NYCSchoolSATScoresModel> satModelList, NYCSchoolsDetailsAdapter.SATListClickListener clickListener) {
        this.satModelList = satModelList;
        this.clickListener = clickListener;
        this.context = context;

    }

    @NonNull
    @Override
    public NYCSchoolsDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.info_recycler_row,parent,false);
        return new MyViewHolder(view);
    }

    //fetching values to Viewholder
    @Override
    public void onBindViewHolder(@NonNull NYCSchoolsDetailsAdapter.MyViewHolder holder, int position) {
        holder.dbn.setText("Dbn is:" + satModelList.get(position).getDbn());
        holder.num_of_sat_test_takers.setText("Number of test takers:" + satModelList.get(position).getNum_of_sat_test_takers());
        holder.sat_critical_reading_avg_score.setText("SAT Reading Average:" + satModelList.get(position).getSat_critical_reading_avg_score());
        holder.sat_math_avg_score.setText("SAT Math Average:" + satModelList.get(position).getSat_math_avg_score());
    }

    @Override
    public int getItemCount() {
        if(satModelList != null ){
            return satModelList.size();
        }
       return 0;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dbn;
        TextView num_of_sat_test_takers;
        TextView sat_critical_reading_avg_score;
        TextView sat_math_avg_score;
        ImageView thumbImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            dbn = itemView.findViewById(R.id.dbn);
            num_of_sat_test_takers = itemView.findViewById(R.id.num_of_sat_test_takers);
            sat_critical_reading_avg_score = itemView.findViewById(R.id.sat_critical_reading_avg_score);
            sat_math_avg_score = itemView.findViewById(R.id.sat_math_avg_score);
            // thumbImage = itemView.findViewById(R.id.thumbImage);
        }
    }
    public interface SATListClickListener {
        void onItemClick(NYCSchoolSATScoresModel NYCSchoolSatScoresModel);
    }

    public void setSATList(List<NYCSchoolSATScoresModel> satModelList) {
        this.satModelList = satModelList;
        notifyDataSetChanged();
    }
}


